#-------------------------------------------------------------------------------
#   DynamoDB -- Backup Reports
#
#      Version  1.0
#      Author:  Blake Huber
#      Instructions:
#           - cloudformation template must be in stalled in HomeRegion
#             first to create IAM policies and the lambda execution role.
#           - Installation in other regions skips creation of IAM resources.
#           - Environment parameter controls the Amazon s3 bucket from which code
#             is extracted for deployment.
#      Limitations:
#           - template only compatible in default HomeRegion (ie, any region where
#             dynamodb source database exists)
#           - template contains stub for 'qa' environment, qa database N/A
#
#-------------------------------------------------------------------------------

AWSTemplateFormatVersion: '2010-09-09'
Description: '__MPCBUILDVERSION__ - MPCBACKUPREPORTS '

#-------------------------------------------------------------------------------
#   PARAMETERS
#-------------------------------------------------------------------------------
Parameters:
  Environment:
    AllowedValues:
        - 'dev'
        - 'qa'
        - 'pr'
    Default: 'dev'
    Description: 'Specify environment from which to deploy code: dev (development),
                  qa (quality assurance), or pr (production) [DEPRECATED]'
    Type: String
  HomeRegion:
    AllowedValues:
        - 'ap-south-1'
        - 'ap-southeast-1'
        - 'ap-southeast-2'
        - 'ap-northeast-1'
        - 'ap-northeast-2'
        - 'ca-central-1'
        - 'eu-central-1'
        - 'eu-west-1'
        - 'eu-west-2'
        - 'sa-east-1'
        - 'us-east-1'
        - 'us-east-2'
        - 'us-west-1'
        - 'us-west-2'
    Default: 'eu-west-1'
    Description: Specify the main ("home" region) in which IAM resources will be deployed first
    Type: String
  TableReadCapacityUnits:
    Description: "Provisioned RCU for Table"
    Type: Number
    MinValue: 4
    Default: 4
    ConstraintDescription" : "Enter a number >=4"
  TableWriteCapacityUnits:
    Description: "Provisioned WCU for Table"
    Type: Number
    MinValue: 4
    Default: 4
    ConstraintDescription" : "Enter a number >=4"
  GSIReadCapacityUnits:
    Description: "Provisioned RCU for GSI"
    Type: Number
    MinValue: 4
    Default: 4
    ConstraintDescription" : "Enter a number >=4"
  GSIWriteCapacityUnits:
    Description: "Provisioned RCU for GSI"
    Type: Number
    MinValue: 4
    Default: 4
    ConstraintDescription" : "Enter a number >=4"


#-------------------------------------------------------------------------------
#   METADATA
#-------------------------------------------------------------------------------
Metadata:
  License: MIT
  Instructions: Set HomeRegion to your default AWS Region.  Deploy to create single DynamoDB Table.
  Version: 0.1
  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
          default: Prerequisites
      Parameters:
          - HomeRegion
    - Label:
          default: Code
      Parameters:
          - Environment
    - Label:
          default: DynamoDB Provisioned Capacity
      Parameters:
          - TableReadCapacityUnits
          - TableWriteCapacityUnits
          - GSIReadCapacityUnits
          - GSIWriteCapacityUnits
    ParameterLabels:
      HomeRegion:
          default: Main Resource Region
      Environment:
          default: Code Origin
      TableReadCapacityUnits:
          default: Table RCU
      TableWriteCapacityUnits:
          default: Table WCU
      GSIReadCapacityUnits:
          default: GSI RCU
      GSIWriteCapacityUnits:
          default: GSI WCU


#-------------------------------------------------------------------------------
#   CONDITIONS
#-------------------------------------------------------------------------------
Conditions:
  CreateIAM:
      Fn::Equals:
          - !Ref "AWS::Region"
          - !Ref HomeRegion


#-------------------------------------------------------------------------------
#   MAPPINGS
#-------------------------------------------------------------------------------
Mappings:
    EnvironmentMap:
        '102512488663':
            "Env": "dev"
        '935229214006':
            "Env": "qa"
        '872277419998':
            "Env": "pr"


#-------------------------------------------------------------------------------
#   RESOURCES
#-------------------------------------------------------------------------------
Resources:
  DDBTable:
    Type: "AWS::DynamoDB::Table"
    TableName: BackupReportsDB
    Properties:
      AttributeDefinitions:
        -
          AttributeName: "Region"
          AttributeType: "S"
        -
          AttributeName: "AccountName"
          AttributeType: "S"
        -
          AttributeName: "Hostname"
          AttributeType: "S"
        -
          AttributeName: "EndDateTime"
          AttributeType: "S"
        -
          AttributeName: "Policy"
          AttributeType: "S"
        -
          AttributeName: "BackupType"
          AttributeType: "S"
        -
          AttributeName: "Success"
          AttributeType: "S"
        -
          AttributeName: "Application"
          AttributeType: "S"
      KeySchema:
        -
          AttributeName: "VolumeId"
          KeyType: "HASH"
        -
          AttributeName: "StartDateTime"
          KeyType: "RANGE"
      GlobalSecondaryIndexes:
        -
          IndexName: "GSI"
          KeySchema:
            -
              AttributeName: "StartDateTime"
              KeyType: "HASH"
            -
              AttributeName: "Hostname"
              KeyType: "RANGE"
          Projection:
            NonKeyAttributes:
              - "Region"
              - "AccountName"
              - "Policy"
              - "Hostname"
              - "Success"
              - "Application"
              - "BackupType"
              - "EndDateTime"
            ProjectionType: "INCLUDE"
          ProvisionedThroughput:
            ReadCapacityUnits:
                - !Ref GSIReadCapacityUnits
            WriteCapacityUnits:
                - !Ref GSIWriteCapacityUnits
      LocalSecondaryIndexes:
        -
          IndexName: "TableLSI"
          KeySchema:
            -
              AttributeName: "VolumeId"
              KeyType: "HASH"
            -
              AttributeName: "EndDateTime"
              KeyType: "RANGE"
          Projection:
            NonKeyAttributes:
              - "Region"
              - "Hostname"
              - "Success"
              - "Application"
            ProjectionType: "INCLUDE"
      ProvisionedThroughput:
        ReadCapacityUnits:
            - !Ref TableReadCapacityUnits
        WriteCapacityUnits:
            - !Ref TableWriteCapacityUnits
  WriteCapacityScalableTarget:
    Type: "AWS::ApplicationAutoScaling::ScalableTarget"
    Properties:
      MaxCapacity: 2
      MinCapacity: 2
      ResourceId: !Join
        - /
        - - table
          - !Ref DDBTable
      RoleARN: !If [CreateIAM, !GetAtt ScalingRole.Arn, !Join [":", ["arn:aws:iam:", !Ref "AWS::AccountId", "role/SR-DynamoDBScaling"]]]
      ScalableDimension: dynamodb:table:WriteCapacityUnits
      ServiceNamespace: dynamodb
  ScalingRole:
    Type: "AWS::IAM::Role"
    Condition: CreateIAM
    Properties:
      RoleName:  SR-DynamoDBScaling
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          -
            Effect: "Allow"
            Principal:
              Service:
                - application-autoscaling.amazonaws.com
            Action:
              - "sts:AssumeRole"
      Path: "/"
      Policies:
        -
          PolicyName: "iampolicy-DynamoDBAutoScaling"
          PolicyDocument:
            Version: "2012-10-17"
            Statement:
              -
                Effect: "Allow"
                Action:
                  - "dynamodb:DescribeTable"
                  - "dynamodb:UpdateTable"
                  - "cloudwatch:PutMetricAlarm"
                  - "cloudwatch:DescribeAlarms"
                  - "cloudwatch:GetMetricStatistics"
                  - "cloudwatch:SetAlarmState"
                  - "cloudwatch:DeleteAlarms"
                Resource: "*"
  WriteScalingPolicy:
    Type: "AWS::ApplicationAutoScaling::ScalingPolicy"
    Properties:
      PolicyName: WriteAutoScalingPolicy
      PolicyType: TargetTrackingScaling
      ScalingTargetId: !Ref WriteCapacityScalableTarget
      TargetTrackingScalingPolicyConfiguration:
        TargetValue: 50.0
        ScaleInCooldown: 60
        ScaleOutCooldown: 60
        PredefinedMetricSpecification:
          PredefinedMetricType: DynamoDBWriteCapacityUtilization


#-------------------------------------------------------------------------------
#   OUTPUTS
#-------------------------------------------------------------------------------
Outputs:
  AccountIdentifier:
    Description: AWS Account Number derived from AccountId pseudo-parameter
    Value: !Ref "AWS::AccountId"
  CodeEnvironment:
    Description: Code environment deployed
    Value: !Ref Environment
