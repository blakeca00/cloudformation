* * *
#  AWS CloudFormation Template Library
* * * 

## Summary 

This repo is a library sample json templates for use with [Amazon Web Services' CloudFormation](http://aws.amazonaws.com/cloudformation) Service.  It is a collection of templates taken from various sources including, but not limited to, sample templates provided by AWS, independent developers, and the repo owner.  Where possible, template authors are noted in the "Description" section of each template.  

Templates have been appended with .json extension to enable json syntax highlighting for ease of use when viewing source via the public repository provider.  This extension will not affect execution of the template.

* * *

## Copyright
 
 All works contained herein copyrighted via below author unless work is explicitly noted by an alternate author.
 
 Author:    Blake Huber 2017 
 
* * *

## Disclaimer

*Templates are provided "as is". No liability is assumed by either the template's originating author nor this repo's owner for their use at AWS or any other facility. Furthermore, running these templates at AWS will incur monetary charges, and in some cases, charges may be substantial. Charges are the sole responsibility of the account holder executing any templates copied from this library.*

* * * 

